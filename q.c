#include <stdio.h>

struct student {
    char firstname[50];
    char subject[20];
    float marks;
};

int main()
{
   int i,n;
   printf("Enter number of students: ");
   scanf("%d" ,&n);
   struct student s[n];
    for(i=0; i<n; i++)
   {
       printf("\nEnter name: ");
       scanf("%s" ,s[i].firstname);
       printf("Enter subject: ");
       scanf("%s" ,s[i].subject);
       printf("Enter marks: ");
       scanf("%f" ,&s[i].marks);
}
printf("\t\n Records of the students\n");
for(i=0; i<n; i++)
{
    printf("\t\nStudent name: %s\n" ,s[i].firstname);
    printf("\tSubject: %s\n" ,s[i].subject);
    printf("\tMarks: %0.2f\n" ,s[i].marks);
}
    return 0;
}
